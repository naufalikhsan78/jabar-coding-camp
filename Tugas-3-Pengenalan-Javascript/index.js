// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var ketiga = pertama.substring(0,5);
var keempat = pertama.substr(12,7);
var kelima = ketiga.concat(keempat);
var keenam = kedua.substring(0,8);
var ketujuh = kedua.substr(8,10);
var kedelapan = ketujuh.toUpperCase();
var kesembilan = keenam.concat(kedelapan);
console.log(kelima.concat(kesembilan));

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var strkataPertama = parseInt(kataPertama);
var strkataKedua = parseInt(kataKedua);
var strkataKetiga = parseInt(kataKetiga);
var strkataKeempat = parseInt(kataKeempat);
var hasil = (strkataKetiga*strkataKedua)+(strkataPertama+strkataKeempat);
console.log(hasil);

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0,3);
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25);
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata kelima: ' + kataKelima);
