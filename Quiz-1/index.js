function jumlah_kata(str) {
  return str.split(" ").length;
}

var kalimat_1 = jumlah_kata("Halo nama saya Mohammad Naufal Ikhsan Rizaldy");
var kalimat_2 = jumlah_kata("Hi Apa Kabar");
var strKata1 = String(kalimat_1);
var strKata2 = String(kalimat_2);
console.log(strKata1);
console.log(strKata2);
