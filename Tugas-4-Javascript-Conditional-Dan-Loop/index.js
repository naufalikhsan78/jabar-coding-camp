//soal 1
var nilai = 75;
if (nilai >= 85){
	console.log("A");
}
else if (nilai >= 75 && nilai < 85){
	console.log("B");
}
else if (nilai >= 65 && nilai < 75){
	console.log("C");
}
else if (nilai >= 55 && nilai <65){
	console.log("D");
}
else {
	console.log("E");
}

// soal 2
var tanggal = 21;
var bulan = 5;
var tahun = 2000;
switch (bulan) {
case 1	: {console.log('21 Januari 2000'); break;}
case 2	: {console.log('21 Februari 2000'); break;}
case 3	: {console.log('21 Maret 2000'); break;}
case 4	: {console.log('21 April 2000'); break;}
case 5	: {console.log('21 Mei 2000'); break;}
case 6	: {console.log('21 Juni'); break;}
case 7	: {console.log('21 Juli 2000'); break;}
case 8	: {console.log('21 Agustus 2000'); break;}
case 9	: {console.log('21 September 2000'); break;}
case 10	: {console.log('21 Oktober 2000'); break;}
case 11	: {console.log('21 November 2000'); break;}
case 12	: {console.log('21 Desember 2000'); break;}
default	: {console.log('Masukan bulan lahir 1-12');}
}

// soal 3
var a = "";
var m = "3";
for (var i = 1; i <= m; i++){
	a += "#";
	console.log(a);
}

// soal 4
// menyerah pada soal no.4